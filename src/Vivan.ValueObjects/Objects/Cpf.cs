﻿using Vivan.Extensions.StringExtensions;
using Vivan.ValueObjects.Validators;
using System;
using System.Collections.Generic;

namespace Vivan.ValueObjects.Objects
{
    public class Cpf : ValueObject
    {
        public readonly string Numero;

        private Cpf() { }

        public Cpf(string cpf)
        {
            if (string.IsNullOrEmpty(cpf))
            {
                cpf = string.Empty;
            }

            Numero = cpf.ToOnlyNumbers();

            Validate();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Cpf cpf))
            {
                return false;
            }

            return Numero == cpf.Numero;
        }

        public override int GetHashCode()
        {
            return Numero.GetHashCode();
        }

        public override string ToString()
        {
            return Numero != null ? Convert.ToUInt64(Numero).ToString(@"000\.000\.000\-00") : string.Empty;
        }

        public sealed override bool Validate()
        {
            return UseValidator(this, new CpfValidator());
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Numero;
        }
    }
}