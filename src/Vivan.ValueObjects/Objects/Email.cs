﻿using Vivan.ValueObjects.Validators;
using System.Linq;
using System.Collections.Generic;

namespace Vivan.ValueObjects.Objects
{
    public class Email : ValueObject 
    {
        public readonly string Completo;

        public readonly string Dominio;

        public readonly string Prefixo;

        protected Email()
        {

        }
        public Email(string email)
        {
            Completo = email;

            Validate();

            if (IsValid)
            {
                var x = Completo.Split('@');

                Dominio = x.Last();
                Prefixo = x.First();
            }
        }

        public override bool Validate()
        {
            return UseValidator(this, new EmailValidator());
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Completo;
            yield return Dominio;
            yield return Prefixo;
        }

        public static implicit operator Email(string email)
        {
            return new Email(email);
        }
    }
}
