﻿using Vivan.ValueObjects.Validators;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Vivan.ValueObjects.Objects
{
    public class Telefone : ValueObject
    {
        private const string OnlyNumbersPattern = @"[^.0-9]";

        public readonly string Numero;

        public readonly int Ddd;

        public readonly int SemDdd;

        public string Formatado => FormatarPtBr();
        protected Telefone() { }
        public Telefone(string number)
        {
            if (string.IsNullOrEmpty(number))
            {
                number = string.Empty;
            }

            Numero = Regex.Replace(number, OnlyNumbersPattern, string.Empty);

            Validate();

            if (!IsValid)
            {
                return;
            }

            Ddd = Convert.ToInt32(Numero.Substring(0, 2));
            SemDdd = Convert.ToInt32(Numero.Substring(2));
        }

        public sealed override bool Validate()
        {
            return UseValidator(this, new TelefoneValidator());
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var phone = obj as Telefone;

            if (ReferenceEquals(null, phone))
            {
                return false;
            }

            return Numero == phone.Numero;
        }

        public override int GetHashCode()
        {
            return Numero.GetHashCode();
        }

        public static implicit operator Telefone(string number)
        {
            return new Telefone(number);
        }

        private string FormatarPtBr()
        {
            long.TryParse(Numero, out var number);

            if (number == 0)
            {
                return string.Empty;
            }

            return Numero.Length == 10 ? $"{number:(##) ####-####}" : $"{number:(##) #####-####}";
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
