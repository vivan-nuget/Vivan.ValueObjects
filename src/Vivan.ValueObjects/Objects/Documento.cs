﻿using System.Collections.Generic;
using Vivan.Extensions.StringExtensions;

namespace Vivan.ValueObjects.Objects
{
    public class Documento : ValueObject
    {
        public char TipoPessoa { get; private set; }
        public bool PessoaFisica { get; private set; }
        public bool PessoaJuridica { get; private set; }
        public string Numero { get; private set; }
        public string NumeroFormatado { get; private set; }

        protected Documento() { }
        public Documento(string documetoNumero)
        {
            var numero = documetoNumero.ToOnlyNumbers();

            if (numero.Length == 11)
            {
                CarregarInformacoesCpf(numero);
            }
            else if (numero.Length == 14)
            {
                CarregarInformacoesCnpj(numero);
            }
            else
            {
                AddNotification("Documento", "Documento informado é inválido.");
            }
        }

        private void CarregarInformacoesCpf(string numero)
        {
            var cpf = new Cpf(numero);

            if (cpf.IsInvalid)
            {
                AddNotifications(cpf.ValidationResult);
                return;
            }

            Numero = cpf.Numero;
            PessoaFisica = true;
            PessoaJuridica = false;
            TipoPessoa = 'F';

            NumeroFormatado = cpf.ToString();
        }

        private void CarregarInformacoesCnpj(string numero)
        {
            var cnpj = new Cnpj(numero);

            if (cnpj.IsInvalid)
            {
                AddNotifications(cnpj.ValidationResult);
                return;
            }

            Numero = cnpj.Numero;
            PessoaJuridica = true;
            PessoaFisica = false;
            TipoPessoa = 'J';

            NumeroFormatado = cnpj.ToString();
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return TipoPessoa;
            yield return PessoaFisica;
            yield return PessoaJuridica;
            yield return Numero;
            yield return NumeroFormatado;
        }
    }
}
