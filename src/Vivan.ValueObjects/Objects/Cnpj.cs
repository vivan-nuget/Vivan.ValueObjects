﻿using Vivan.ValueObjects.Validators;
using System;
using Vivan.Extensions.StringExtensions;
using System.Collections.Generic;

namespace Vivan.ValueObjects.Objects
{
    public class Cnpj : ValueObject
    {
        public readonly string Numero;

        protected Cnpj() { }

        public Cnpj(string cnpj)
        {
            if (string.IsNullOrEmpty(cnpj))
            {
                cnpj = string.Empty;
            }

            Numero = cnpj.ToOnlyNumbers();

            Validate();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Cnpj cnpj))
            {
                return false;
            }

            return Numero == cnpj.Numero;
        }

        public override int GetHashCode()
        {
            return Numero.GetHashCode();
        }

        public override string ToString()
        {
            return Numero != null ? Convert.ToUInt64(Numero).ToString(@"00\.000\.000\/0000\-00") : string.Empty;
        }

        public sealed override bool Validate()
        {
            return UseValidator(this, new CnpjValidator());
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Numero;
        }
    }
}