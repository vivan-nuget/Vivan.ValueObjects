﻿using Vivan.ValueObjects.Validators;
using System;
using System.Collections.Generic;

namespace Vivan.ValueObjects.Objects
{
    public class DataNascimento : ValueObject
    {
        public readonly DateTime Data;

        public int Idade => CalcularIdade();

        protected DataNascimento()
        {

        }
        public DataNascimento(DateTime dataNascimento)
        {
            Data = dataNascimento;

            CalcularIdade();
            Validate();
        }

        private int CalcularIdade()
        {
            if (IsInvalid)
                return default(int);

            DateTime today = DateTime.Today;

            int idade = today.Year - Data.Year;

            if (Data > today.AddYears(-idade))
                idade--;

            return idade;
        }

        public override bool Validate()
        {
            return UseValidator(this, new DataNascimentoValidator());
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
                return true;

            var dataNascimento = obj as DataNascimento;

            if (ReferenceEquals(null, dataNascimento))
                return false;

            return Data == dataNascimento.Data;
        }

        public override int GetHashCode()
        {
            return Data.Date.GetHashCode();
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Data;
        }

        public static implicit operator DataNascimento(DateTime dataNascimento)
        {
            return new DataNascimento(dataNascimento);
        }
    }
}
