﻿using FluentValidation;
using FluentValidation.Validators;
using Vivan.ValueObjects.Objects;

namespace Vivan.ValueObjects.Validators
{
    public class CpfValidator : AbstractValidator<Cpf>
    {
        public CpfValidator()
        {
            RuleFor(cpf => cpf.Numero)
                .Custom(CpfIsValid);
        }

        private void CpfIsValid(string cpf, CustomContext context)
        {
            if (cpf == null)
            {
                context.AddFailure("CPF", "O CPF informado é inválido");
                return;
            }

            int[] multiplicador1 = { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            var multiplicador2 = new[] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            
            if (cpf.Length != 11)
            {
                context.AddFailure("CPF", "O CPF informado é inválido");
                return;
            }

            var tempCpf = cpf.Substring(0, 9);
            var soma = 0;

            for (var i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            
            var resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            
            var digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            
            for (var i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
           
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            
            digito += resto.ToString();

            if (!cpf.EndsWith(digito))
            {
                context.AddFailure("CPF", "O CPF informado é inválido");
            }
        }
    }
}