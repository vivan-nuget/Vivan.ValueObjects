﻿using FluentValidation;
using Vivan.Extensions.DateTimes;
using Vivan.ValueObjects.Objects;
using System;

namespace Vivan.ValueObjects.Validators
{
    internal class DataNascimentoValidator : AbstractValidator<DataNascimento>
    {
        private const string _phonePattern = @"^\d{11}$";

        public DataNascimentoValidator()
        {
            RuleFor(dataNascimento => dataNascimento.Data.Date)
                .Must(SerValida).WithMessage("A 'Data de Nascimento' informada é inválida.");
        }

        private bool SerValida(DateTime dataNascimento)
        {
            return dataNascimento.IsNotDefault();
        }
    }
}
