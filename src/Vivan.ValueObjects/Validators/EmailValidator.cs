﻿using FluentValidation;
using Vivan.ValueObjects.Objects;

namespace Vivan.ValueObjects.Validators
{
    public class EmailValidator : AbstractValidator<Email>
    {
        public EmailValidator()
        {
            RuleFor(e => e.Completo)
                .EmailAddress()
                .WithMessage("O email informado é invalido.");
        }
    }
}
