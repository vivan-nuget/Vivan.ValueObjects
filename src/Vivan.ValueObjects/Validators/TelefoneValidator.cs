﻿using FluentValidation;
using Vivan.ValueObjects.Objects;
using System.Text.RegularExpressions;

namespace Vivan.ValueObjects.Validators
{
    internal class TelefoneValidator : AbstractValidator<Telefone>
    {
        private const string _telefonePattern = @"^\d{10,11}$";

        public TelefoneValidator()
        {
            RuleFor(telefone => telefone.Numero)
                .Must(SerValido).WithMessage("O 'Numero' de telefone informado é inválido.");
        }

        private bool SerValido(string number)
        {
            return Regex.IsMatch(number, _telefonePattern, RegexOptions.IgnoreCase);
        }
    }
}
