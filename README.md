# Vivan.ValueObjects

Biblioteca que possui um conjunto de métodos de ValueObjects comumente usados em aplicações para agilizar o dsenvolvimento...

## Instalação

Instalar o package **_'Vivan.ValueObjects'_** através do Nuget Package Manager (Servidor da Vivan).

## Dependências

* Vivan.Extensions
* Vivan.Notifications

## Recursos
* Classe ValueObject
* Telefone
* DataNascimento

`Todos os Value Objects herdam da classe "ValueObject" que implementa a classe Validatable.`

`Classes Validatables fornecem lista de notificações e um atributo IsValid que informa se a mesma é válida ou não.`

## Utilizando os recursos do Telefone

O Value Object **Telefone** recebe como parâmetro de entrada uma string contendo o número do telefone e fornece um objeto com as seguintes informações:

- Número completo (Telefone.Numero) 
- Número do DDD (Telefone.Ddd)
- Número sem o Ddd (Telefone.SemDdd)
- Número formatado (Telefone.Formatado)

```c#
var telefone = new Telefone("(51) 99715-1205");
Console.WriteLine(telefone.Ddd);       //51
Console.WriteLine(telefone.SemDdd);    //997151205
Console.WriteLine(telefone.Formatado); //(51) 99715-1205
Console.WriteLine(telefone.Numero);    //51997151205

//Formas de construir o Value Object Telefone:
var telefone1 = new Telefone("(51) 99715-1205");
var telefone2 = new Telefone("(51) 997151205");
var telefone3 = new Telefone("51 99715-1205");
var telefone4 = new Telefone("51 99715 1205");
var telefone5 = new Telefone("51 997151205");
var telefone6 = new Telefone("51997151205");

Telefone telefoneImplicito1 = "(51) 99715-1205";
Telefone telefoneImplicito2 = "(51) 997151205";
Telefone telefoneImplicito3 = "51 99715-1205";
Telefone telefoneImplicito4 = "51 99715 1205";
Telefone telefoneImplicito5 = "51 997151205";
Telefone telefoneImplicito6 = "51997151205";

//Exibindo erros do telefone:
var telefoneInvalido = new Telefone("(51) 9972205");

if (!telefoneInvalido.IsValid
{
    foreach (var notification in telefoneInvalido.ValidationResult.Errors)
    {
        Console.WriteLine(notification);
    }
    //Mensagem do console:
    //O 'Numero' de telefone informado é inválido.
}
```

## Utilizando os recursos do DataNascimento

O Value Object **DataNascimento** recebe como parâmetro de entrada uma data e fornece um objeto com as seguintes informações:

- Data de Nascimento (DataDeNascimento.Data) 
- Idade (DataDeNascimento.Idade)

```c#
//O VO data de nascimento pode ser atribuido implicitamente ou por uma instância
DataNascimento dataNascimento = DateTime.Now.AddYears(-18);
var dataDeNascimento = new DataNascimento(DateTime.Now.AddYears(-18));

//Atributos
Console.WriteLine(dataDeNascimento.Data);   //25/04/2001
Console.WriteLine(dataDeNascimento.Idade);  //17

//Exibindo erros da Data:
var dataDeNascimentoInvalida = new DataNascimento(default(DateTime));

if (!dataDeNascimentoInvalida.IsValid)
{
    foreach (var notification in dataDeNascimentoInvalida.ValidationResult.Errors)
    {
        Console.WriteLine(notification);
    }

    //Mensagem do Console:
    //A 'Data de Nascimento' informada é inválida.
}
```